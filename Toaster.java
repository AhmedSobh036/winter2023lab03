public class Toaster
{
	public String brand;
	public String color;
	public int numOfSlots;
	
	public void printBrand()
	{
		System.out.println("This method prints the brand of the toaster which is: " + this.brand);
	}
	
	public void toastBread(int numOfBread)
	{
		if(numOfBread > numOfSlots)
		{
			System.out.println("That's way too much bread");
		}
		else
		{
			System.out.println("Bread has been inserted and is currently getting toasted.");
		}
	}
}