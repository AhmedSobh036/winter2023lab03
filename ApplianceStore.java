import java.util.Scanner;
public class ApplianceStore
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Toaster[] toasters = new Toaster[4];
		for(int i=0, num=1; i<toasters.length; i++, num++)
		{
			toasters[i] = new Toaster();
			System.out.println("Enter the brand of toaster number " + num + " .");
			toasters[i].brand = scan.nextLine();
			System.out.println("Enter the color of toaster number " +num + " .");
			toasters[i].color = scan.nextLine();
			System.out.println("Enter the number of slots that toaster number " + num + " has.");
			toasters[i].numOfSlots = Integer.parseInt(scan.nextLine());
		}
		System.out.println("The brand of the last toaster is " + toasters[3].brand + " . The color of the last toaster is " + toasters[3].color + " . The number of slots the last toaster has is " + toasters[3].numOfSlots + " .");
		toasters[2].printBrand();
		System.out.println("How much bread do you want to put into the last toaster?");
		int bread = scan.nextInt();
		toasters[3].toastBread(bread);
	}
}